import React, { Component } from 'react';
import './AlphaButtons.css';

class AlphaButtons extends Component {



    render(){

        const letters = "abcdefghijklmnopqrstuvwxyz".split("");

        const buttons = letters.map(ltr => (
            <button
                className="Hangman-letter-button"
                key={ltr}
                value={ltr}
                onClick={this.props.handleGuess}
                disabled={this.props.guessed.has(ltr)}
            >
                {ltr}
            </button>
        ));

        return(
            <div>

                { this.props.nWrong < this.props.maxWrong ? <p className='Hangman-btns'>{buttons}</p> : null }
            </div>
        )

    };
}

export default AlphaButtons;