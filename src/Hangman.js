import React, { Component } from "react";
import AlphaButtons from './AlphaButtons';
import "./Hangman.css";
import img0 from "./0.jpg";
import img1 from "./1.jpg";
import img2 from "./2.jpg";
import img3 from "./3.jpg";
import img4 from "./4.jpg";
import img5 from "./5.jpg";
import img6 from "./6.jpg";
import { randomWord } from './words';

class Hangman extends Component {
  /** by default, allow 6 guesses and use provided gallows images. */
  static defaultProps = {
    maxWrong: 6,
    images: [img0, img1, img2, img3, img4, img5, img6]
  };

  constructor(props) {
    super(props);
    this.state = { victory: false, nWrong: 0, guessed: new Set(), answer: "apple"};
    this.handleGuess = this.handleGuess.bind(this);
    this.handleRestart = this.handleRestart.bind(this);
  }

  /** guessedWord: show current-state of word:
    if guessed letters are {a,p,e}, show "app_e" for "apple"
  */
  guessedWord() {
    return this.state.answer
      .split("")
      .map(ltr => (this.state.guessed.has(ltr) ? ltr : "_"));
  }


  checkVictory() {
    let victory = true;
    const answer = this.state.answer.split("");
    answer.map(ltr => {
      if(!this.state.guessed.has(ltr)){
        victory = false;
      }
      return victory;
    })
    if(victory){

      this.setState(prev => { return { victory: !prev.victory }})
    }
  }

  /** handleGuest: handle a guessed letter:
    - add to guessed letters
    - if not in answer, increase number-wrong guesses
  */
  handleGuess(evt) {
    let ltr = evt.target.value;
    console.log(ltr);
    this.setState(st => ({
      guessed: st.guessed.add(ltr),
      nWrong: st.nWrong + (st.answer.includes(ltr) ? 0 : 1),
    }));

    setTimeout(() => {
      this.checkVictory();
    }, 100);

  }


  handleRestart() {
    this.setState({
      nWrong: 0, 
      guessed: new Set(), 
      answer: randomWord(),
      victory: false
    })
  }

  /** render: render game */
  render() {
    return (
      <div className='Hangman'>
        <h1>Hangman</h1>
        {this.state.nWrong < this.props.images.length ? <img alt={this.state.nWrong + ' of ' + this.props.maxWrong + " wrong"} src={this.props.images[this.state.nWrong]} /> : null} 
        {this.state.nWrong < this.props.maxWrong ? <p>Wrong Guesses are: {this.state.nWrong}</p> : <h1 className='Hangman-gameOver'>Game Over</h1> }
        {this.state.victory || this.state.nWrong >= this.props.maxWrong ? <h2>Correct answer is: {this.state.answer}</h2> : null }
        {!this.state.victory && this.state.nWrong < this.props.maxWrong ? <p className='Hangman-word'>{this.guessedWord()}</p> : null}
        {this.state.victory ? <h1>Victory</h1> : null} 
        {!this.state.victory && this.state.nWrong < this.props.maxWrong ? <AlphaButtons maxWrong={this.props.maxWrong} nWrong={this.state.nWrong} handleGuess={this.handleGuess} guessed={this.state.guessed}/> : null }
        {this.state.victory || this.state.nWrong >= this.props.maxWrong ? <button onClick={this.handleRestart} className='Hangman-playAgain-button'>Play Again!!!</button> : null }
        
      </div>
    );
  }
}

export default Hangman;
